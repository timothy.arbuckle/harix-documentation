# HARIX SKILL RDK DOCUMENTATION

## Table of Contents

- [Scope of Application](##scope-of-application)
- [Authentication](#authentication)
- [Control Service](#control-service)
- [Configuration](#configuration)
- [Navigation](#navigation)
- [Speak Service](#speak-service)
- [Natural Language Understanding](#natural-language-understanding)
- [Speech Recognition](#speech-recognition)
- [Image Recognition](#image-recognition)
- [Map Service](#map-service)

## Scope of Application

This document describes a proposal for an implementation of a Robot SDK (RDK).
This RDK would be provided as a library or module to developers and makes it much
easier for them to develop applications with CloudMinds robots. It would hide
most of the tedious work of formatting and sending gRPC messages to HARIX and 
translating the responses. Instead it would appear as a simple native interface.

For this document, the RDK examples will be demonstrated using Python but the
actual RDK could and probably should be implemented in a few languages/platforms
(such as Golang, NodeJS, Android, iOS) based on customers need.

```python
# example of how python rdk code may look
from CloudMindsRDK import Robot

# all HARIX functionality is available within the Robot module
robot = Robot(...)
```

## Proposal for Order of Development

Development of the RDK can be staged to provide capabilities
that can be used while other capabilities are being enabled.
The list below suggests a possible order for implentation. This
order would allow developers to start with a virtual robot for
speech and image recognition before moving to a physical robot
for mapping and navigation.

* Authentication
* Control
* Speak Service
* Speech Recognition
* Configuration
* Image Recognition
* Natural Language Understanding
* Navigation
* Map Service

## Authentication

```python
from CloudMindsRDK import Robot

auth_info = {
    "tenantCode": "",
    "robotId": "",
    "serviceAppId": "",
    "serviceAppKey": "",
    "authType": ""
}

robot = Robot()
robot.ROC.authorize(auth_info)

if not robot.authorized():
    print "Not Authorized"
return
```

## Control Service

### Reboot RCU
```
robot.ControlService.reboot_rcu()
```

### Shutdown RCU
```
robot.ControlService.shutdown_rcu()
```

### Restart App
```
robot.ControlService.restart_app()
```

### Lock RCU Screen
```
robot.ControlService.lock_rcu_screen()
```

### Light RCU Screen
```
robot.ControlService.light_rcu_screen()
```

### Shutdown Robot
```
robot.ControlService.shutdown_robot()
```

### Wakeup Robot
```
robot.ControlService.wakeup_robot()
```

### Reset Robot
```
robot.ControlService.reset_robot()
```

### Reboot Robot
```
robot.ControlService.reboot_robot()
```

### Reconnect Robot
```
robot.ControlService.reconnect_robot()
```

### Attach Charging Pile
```
robot.ControlService.attach_charging_pile()
```

### Detach Charging Pile
```
robot.ControlService.detach_charging_pile()
```

### Get Robot Actions
```
robot.ControlService.get_robot_actions()
```

### Do Intent
```
robot.ControlService.do_intent()
```

### Do Action
```
robot.ControlService.do_action()
```

### Move
```
robot.ControlService.move()
```

### Stop
```
robot.ControlService.stop()
```

### Rotate
```
robot.ControlService.rotate()
```

### Emergency Stop
```
robot.ControlService.emergency_stop()
```

## Configuration

### Config Robot
```
robot.Config.config()
```

### Get Robot State
```
robot.Config.get_state()
```

### Start Record Media
```
robot.Config.start_record_media()
```

### Stop Record Media
```
robot.Config.stop_record_media()
```

### ASR (automatic speech recognition)
```
robot.Config.asr()
```

### FR (facial recognition)
```
robot.Config.fr()
```

### Face Cam
```
robot.Config.face_cam()
```

## Navigation

### Get Map
```
robot.Nav.get_map()
```

### Get Map Location
```
robot.Nav.get_map_location()
```

### Get Patrol Route
```
robot.Nav.get_patrol_route()
```

### Start Patrol
```
robot.Nav.start_patrol()
```

### Pause Patrol
```
robot.Nav.pause_patrol()
```

### Resume Patrol
```
robot.Nav.resume_patrol()
```

### Stop Patrol
```
robot.Nav.stop_patrol()
```

### Enable Calibration Points
```
robot.Nav.enable_calibration_points()
```

### Move To
```
robot.Nav.move_to()
```

### Move By Path
```
robot.Nav.move_by_path()
```

### Set Robot Location
```
robot.Nav.set_location()
```

## Speak Service

### Speak
```
robot.speak()
```

## Natural Language Understanding

### Detect Intent
```
robot.NLU.detect_intent()
```

### Detect Intent Text
```
robot.NLU.detect_intent_text()
```

## Speech Recognition

### Recognize
```
robot.SR.recognize()
```

### Streaming Recognize
```
robot.SR.stream_recognize()
```

## Image Recognition

### Recognize
```
robot.IR.recognize()
```

### Streaming Recognize
```
robot.IR.stream_recognize()
```

## Map Service

#### Upload map information
```
robot.Map.upload()
```

#### Page query map information
```
robot.Map.query()
```

#### Query map information based on geographic location
```
robot.Map.query()
```

#### Get map information details
```
robot.Map.details()
```

#### Modify map information
```
robot.Map.update()
```

#### Delete map information
```
robot.Map.delete()
```

### Layer group related operations
#### Paged query layer grouping
```
robot.Map.LayerGroup.query()
```

#### New layer grouping
```
robot.Map.LayerGroup.add()
```

#### Modify layer grouping
```
robot.Map.LayerGroup.update()
```

#### Delete layer grouping
```
robot.Map.LayerGroup.delete()
```

### Layer information related operations
#### New layer information
```
robot.Map.Layer.add()
```

#### Modify layer information
```
robot.Map.Layer.update()
```

#### Delete layer information
```
robot.Map.Layer.delete()
```

### Location related operations
#### Query the location by page
```
robot.Map.Location.query()
```

#### New location
```
robot.Map.Location.add()
```

#### Edit location
```
robot.Map.Location.update()
```

#### Delete location
```
robot.Map.Location.delete()
```
