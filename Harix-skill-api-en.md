# HARIX SKILL API DOCUMENTATION

## Robot Platform (HARIX) Interface Specification

* Dahao Technology Co, Ltd.
* Document version: _v1.0._
* Revision date: _20190921_
* Software version: _v3.1._

### Scope of application

This document describes the interfaces for third-party services and internal
business platforms to access the HARIX platform. Users can use gRPC and HTTP
REST API in two ways.

**Access**
For information on using the RCU to access the HARIX interface and process,
please refer to the download center attachment file.

### Error

If you find an error, please contact the Harix team in time.
Note: For the official address of the latest document, please click here.

### Release record

Release Time | Parameter type
------------ | --------------
2019-09-27 | Initialization version

## Access Guide

### Letter of Agreement

API calls follow the gRPC protocol, a high-performance, open source, and generic
RPC framework for mobile and HTTP/2 designs.
It relies on HTTP2 and Protocol buffer.

## API Certification

When calling the API service using gRPC or Rest mode, you must include the
authentication parameters authType and authToken in the parameters, which can
be authenticated in the background.
Service generation, please refer to key management for specific methods.

### Date and time specifications

* The date is in YYYY-MM-DD mode, for example, 2019-09-01 means September 1, 2019
* Indicates that the time is in HH:mm:ss mode, for example, 14:26:01 means 14:26:1
* When displaying dates and times at the same time, use YYYY-MM-DD HH:mm:ss with a space between them

### Encoding Format
All strings are encoded in UTF-8.

### Typographical Convention

| Name | Description |
| ---- | ----------- |
| <> | Variable |
| [] | Optional |
| {} | Required |
| &#124; | Mutually exclusive |

### Abbreviations

| Abbreviation | Full English Name | Chinese meaning |
| --- | --- | --- |
| HARI | Human Augmented Robotic Intelligence | Artificially Enhanced Machine Intelligence |
| RCU | Robotic Control Unit | Robot Control Unit (host computer) |
| CCU | Central Control Unit | main unit (median unit) |
| ROC | Robot Operations Center | Robot Operations Center |
| ROSS | Robot Operations Support System | |
| AI | Artificial Intelligence | Artificial Intelligence |
| HI | Human Intelligence | Human Intelligence |
| RI | Robotic Intelligence | Machine Intelligence |
| ML | Machine Learning | Machine Learning |
| DL | Deep Learning | |
| SVision | SmartVision | Smart Vision Product Feature Collection |
| SVoice | SmartVoice System | Natural Language Understanding System |
| SMotion | SmartMotion | Intelligent Motion System |
| SLAM | Simultaneous Localization and Mapping | |
| VSLAM | Visual Simultaneous Localization and Mapping | Visual Synchronization Positioning and Mapping |
| RDM | Robotic Device Management | Robotic Device Management |
| CMS | Content Management System | Content Management System |
| MMO | Massively Multiplayer Online | Massively Multiplayer Online Game |
| ROS | Robot Operating System | |
| APN | Access Point Name | 3G/4G Wireless Network Proprietary Tunnel Security Access Point |
| MMS | Multiple Media Service | Multimedia Distribution Service |
| ASR | Automatic Speech Recognition | automatically converts from speech to text recognition |
| TTS | Text To Speech | completes text-to-speech synthesis |
| NLP | Natural Language Processing | | 
| SCA | Smart Compliant Actuator | Smart Flexible Actuator |
| GRPC | Google Remote Process Control | Google Remote Call System |
| FR | Face Recognition | facial recognition |

## Secret Key Management

### Overview

Before using this series of API interfaces, you need to register users through ROC, and get the information needed to access the interface as shown in the following process. No certified access please

Asking for the service will return the following error message.

| Error Code | Error Message | Description |
| --- | --- | --- | 
| 403 | the user has no authority | account has no access rights |

### Process

sequenceDiagram Developer ->> HARIX: Apply for registration application HARIX->>Developer: Register successfully, provide serviceAppID,

serviceAppKey Developer ->> ROC: /roc/V1/authorize/ (with serviceAppID+serviceAppKey) ROC ->> Developer:

authToken Developer ->> gRPC: Request API (with authToken) Developer ->> ROC: authToken expired /roc/V1/authorize/
(with serviceAppID+serviceAppKey)

### Service registration

Before using the authentication interface, you need to submit a service development application to the HARIX platform. Within 1 working day after submitting the application, the staff will send a service letter.

Please register and open the API usage rights.

Users will get the corresponding (serviceAppID and serviceAppKey)

### Authentication mechanism

The user needs to obtain the Token of the access first before initiating the request. The Token expires for 1 month. If the access is denied, the user can try to obtain a new one.
For a valid Token, see the following interface to get the Token:

**Request agreement**

| Interface Description | Page Query Map Information |
| --- | --- |
| Request URL | http://{host-name}/roc/V1/authorize/ |
| Request Method | POST |

**Header**

| Parameter name | value |
| --- | --- |
| Content-Type | application/json; charset=utf-8 |

**Request parameter**

| Parameter Name | Parameter Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| tenantCode | string | Required | Customer Code |
| robotId | string | optional | robot code |
| serviceAppID | string | Optional | serviceApp ID information |
| serviceAppKey | string |Optional | Key information for the ServiceApp |
| authType | int | required | 1: key auth, 2: basic auth, 3: Hmac auth |

Note: _robotId_ and _serviceApp_ are both selected; currently only _key auth_ is supported.

**Return parameter**

| Parameter Name | Parameter Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| Sys | string | Required | Platform ID, extended use |
| Code | string | Required | The response code of the request |
| Messages | string | Required | response to the request |
| Errors | string | Optional | error message |
| Data | string | Required | Business Data Node |

**Response body JSON**

```json
{
  "sys": "ROC",
  "code": 0 ,
  "messages": "OK",
  "data": {
    "authType": 1 ,
    "authToken": "xxxx "
  },
  "errors": null
}
```
After obtaining the authentication information successfully, the program can store the authType and authToken, and set it in the meta information before accessing the gRPC service. Following generation the code is a call example.

```golang
Type CustomCredential struct{}

Func (c CustomCredential) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
    Return map[string]string {
        "appid": "101010",
        "appkey": "i am key",
    }, nil
}

Func (c CustomCredential) RequireTransportSecurity() bool {
    Return false
}

Func Test_RobtAgent(t *testing.T) {
    Const SERVER_ADDR string = "127.0.0.1:50051"
    Var opts []grpc.DialOption
    Opts = append(opts, grpc.WithInsecure())
    Opts = append(opts, grpc.WithUnaryInterceptor(clientInterceptor))
    Opts = append(opts, grpc.WithPerRPCCredentials(new(CustomCredential)))
    Conn, err := grpc.Dial(SERVER_ADDR, opts...)
    If err != nil {
        log.Fatalf("Cannot connect %v", err)
    }
}
```

# Public Structure

### CommonReqHeader

```
Message CommonReqHeader {
    String tenant_id = 4;
    String user_id = 5;
    String robot_id = 6;
    String robot_type = 7;
    String service_code = 8;
    String seq = 9;
}
```

| Name | Type | Required | Description |
| --- | --- | --- | --- |
| tenant_id | string | - | is the customer code |
| user_id | string | - | is the user account |
| robot_id | string | - | is the robot ID |
| robot_type | string | - | is the robot type |
| service_code | string | - | is the service code |
| seq | string | - | is the serial number |

### CommonRspInfo

```
Message CommonRspInfo {
    Int32 err_code = 1;
    String err_msg = 2;
    String err_detail = 3;
}
```

| Name | Type | Description |
| --- | --- | --- |
| err_code | int32 | error code |
| err_msg | string | error message |
| err_detail | string | error details |

## Common Error Code

When calling the API, CommonRspInfo.ErrCode = 0 means the call is successful,
non-zero values ​​represent failures, and the common code is as
follows.

### Error Code Definition

| Name | Description |
| --- | --- |
| 0 | Successful call |
| 10001 | Location error |
| 10002 | Access restricted, insufficient permissions |
| 10003 | The parameter is empty |
| 11004 | Connection RCC timeout |
| 11005 | Connection ServiceApp timeout |
| 11006 | ServiceApp failed to perform a task |
| 11007 | ServiceApp configuration is incorrect |
| 11008 | ServiceApp call timed out |
| 11009 | ServiceApp call is canceled, please check the configuration |
| 12000 | request was interrupted |
| 20001 | Unknown exception |
| 20002 | return value is null |
| 20003 | Call parameters are incorrect, please check parameters |
| 20004 | internal connection timed out |
| 20005 | dialogflow configuration is incorrect |
| 30001 | parsing parameter error |
| 30002 | parsing returned result error |
| 30100 | Error connecting to the Switch server |
| 30101 | An error occurred while sending data to the Switch server |
| 30102 | Failed to get robot information |
| 30103 | Robot not logged in |
| 30104 | The artificial seat is not online |
| 30105 | Failed to start RCU recording |
| 30106 | Failed to start RTSP video recording |
| 30107 | Failed to start video recording |
| 30108 | Failed to stop RCU video recording |
| 30109 | Stop RTSP video recording failed |
| 30110 | Stop video recording failed |
| 30111 | Robot has no camera information |
| 30112 | Failed to send command to robot |
| 30200 | Service App configuration is incorrect |
| 30201 | An error occurred while calling the Service App |
| 30300 | failed to communicate with MMO |
| 40001 | TTS module unknown exception |
| 40002 | Incoming audio stream is empty |
| 40003 | tts parameter is incorrect |
| 40004 | audio file url is empty |
| 40005 | Failed to get audio |
| 70001 | Image recognition module unknown error |
| 70002 | Image recognition API call failed with insufficient permissions. |
| 70003 | missing parameter |
| 71004 | Image module failed to connect to RCC |
| 71005 | Image Module Connection ServiceApp Timeout |
| 71006 | Calling ServiceApp execution method failed |
| 71007 | ServiceApp configuration is incorrect |
| 71008 | ServiceApp call timed out |
| 71009 | ServiceApp call failed, please check the configuration |
| 71100 | RCC module configuration is incorrect |
| 71101 | Vision module not configured |
| 71200 | ServiceApp or Vision module is not configured correctly. |

## Robot Platform API

* Robot Control
* Robot Configuration
* Robot Navigation
* Voice broadcast
* Event Notification

### Robot Control Interface

#### index

* RobotControlService (interface)
  * RebootRcu (method)
  * ShutdownRcu (method)
  * RestartApp (method)
  * LockRcuScreen (method)
  * LightRcuScreen (method)
  * ShutdownRobot (method)
  * WakeupRobot (method)
  * ResetRobot (method)
  * RebootRobot (method)
  * ReconnectRobot (method)
  * AttachChargingPile (method)
  * DetachChargingPile (method)
  * GetRobotActions (method)
  * DoIntent (method)
  * DoAction (method)
  * Move (method)
  * Stop (method)
  * Rotate (method)
  * EmergencyStop (method)
* RobotRequest (message)
* RobotResponse (message)
* IntentRequest (message)
* ActionRequest (message)
* MoveRequest (message)
* MapModel (message)
* VelocityModel (message)
* EmergencyStopRequest (message)

#### RobotControlService

##### RebootRcu

Rpc RebootRcu (RobotRequest) returns (RobotResponse)
Restart the RCU.

##### ShutdownRcu

Rpc ShutdownRcu (RobotRequest) returns (RobotResponse)
Turn off the RCU.

##### RestartApp

Rpc RestartApp (RobotRequest) returns (RobotResponse)
Restart the RCU APP.

##### LockRcuScreen

Rpc LockRcuScreen (RobotRequest) returns (RobotResponse)
Control the Rcu lock screen.

##### LightRcuScreen

Rpc LightRcuScreen (RobotRequest) returns (RobotResponse)
Light up the Rcu screen.

##### ShutdownRobot

Rpc ShutdownRobot (RobotRequest) returns (RobotResponse)
Turn off the robot.

##### WakeupRobot

Rpc WakeupRobot (RobotRequest) returns (RobotResponse)
Wake up the robot.

##### ResetRobot

Rpc ResetRobot (RobotRequest) returns (RobotResponse)
Reset the robot.

##### RebootRobot

Rpc RebootRobot (RobotRequest) returns (RobotResponse)
Restart the robot.

##### ReconnectRobot

Rpc ReconnectRobot (RobotRequest) returns (RobotResponse)
Reconnect the robot.

##### AttachChargingPile

Rpc AttachChargingPile (RobotRequest) returns (RobotResponse)
Control the robot to charge.

##### DetachChargingPile

Rpc DetachChargingPile (RobotRequest) returns (RobotResponse)
Control the robot to cancel charging.

##### GetRobotActions

Rpc GetRobotActions (RobotRequest) returns (RobotResponse)
Inform the RCU to report the action list and dance list.

##### DoIntent

Rpc DoIntent (IntentRequest) returns (RobotResponse)
Control the robot to perform tasks with a fixed intent.

##### DoAction

Rpc DoAction (ActionRequest) returns (RobotResponse)
Control the robot to perform an action. The robot has some built-in actions, and this list can be obtained by executing the GetRobotActions method.

##### Move

Rpc Move (MoveRequest) returns (RobotResponse)
mobile.

##### Stop

Rpc Stop (RobotRequest) returns (RobotResponse)
Control the robot to stop moving.

##### Rotate

Rpc Rotate (RotateRequest) returns (RobotResponse)
Control the robot to rotate.

##### EmergencyStop

Rpc EmergencyStop (EmergencyStopRequest) returns (RobotResponse)
Control the robot to stop.

#### RobotRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |

#### RobotResponse

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_rsp_info | common.CommonRspInfo | public return header |

#### IntentRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| intentType | string | The optional values ​​are showImage, showBarcode playAudio, playVideo, renderHtml |
| params | string | action additional parameters |

#### ActionRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| action | string | action name |
| params | map | action additional parameters |

#### MoveRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| param | MapModel or VelocityModel | speed parameter |

#### MapModel

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| angle | double | angle |
| speed | ​​int32 | speed |
| distance | int32 | distance |

#### VelocityModel

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| vx | double | x direction speed |
| vy | int32 | y direction speed |
| vw | int32 | angular velocity |

#### EmergencyStopRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | |
| emergencyStopSwitch | bool | true: open emergency stop, false: close emergency stop |

### Robot Configuration Interface
#### index

* RobotConfigService (interface)
  * ConfigRobot (method)
  * GetRobotState (method)
  * ConfigStartRecordMedia (method)
  * ConfigStopRecordMedia (method)
  * ConfigAsr (method)
  * ConfigFr (method)
  * ConfigFaceCam (method)
* ConfigRobotRequest (message)
* RobotBaseResponse (message)
* GetRobotStateRequest (message)
* ClientRecordConfig (message)
* ClientRecordStop (message)
* ConfigAsrRequest (message)
* ConfigFrRequest (message)
* ConfigFaceCamRequest (message)

#### RobotConfigService

This interface provides APIs related to HARIX robot settings. The settings are divided into two types.

Platform function settings
Contains parameters such as startup platform video recording, platform image recognition, etc.

Robot related settings
Contains setting robot camera, status related parameters

##### ConfigRobot

Rpc ConfigRobot (ConfigRobotRequest) returns (RobotConfigResponse)
Used to set various basic configuration parameters of the robot.

##### GetRobotState

Rpc GetRobotState (GetRobotStateRequest) returns (RobotConfigResponse)
Used to get the status and configuration parameters specified by the RCU and the robot.

##### ConfigAsr

Rpc ConfigAsr (ConfigAsrRequest) returns (RobotConfigResponse)
Turns the voice recognition function of the robot on/off.

##### ConfigFr

Rpc ConfigFr (ConfigFrRequest) returns (RobotConfigResponse)
Turn on/off the video recognition function of your phone.

##### ConfigFaceCam

Rpc ConfigFaceCam (ConfigFaceCamRequest) returns (RobotConfigResponse)

Turns on/off the image recognition function of the robot body camera. Usually, the face recognition function is implemented by the RCU camera, but in some cases,
To use the robot body camera, you can call this method to set it up.

Take the Pepper robot as an example. The camera number 0 is the overhead camera, 1 is the mouth camera, and 2 is the chest or RCU camera. Other types of machines
The person can be numbered separately according to the specific situation.

#### ConfigStartRecordMedia

Rpc StartRecorder (ClientRecordConfig) returns (RobotConfigResponse)
Start the platform to record the real-time media stream uploaded by the robot.

#### ConfigStopRecordMedia

Rpc StopRecorder (ClientRecordStop) returns (RobotConfigResponse)
Stop the platform to record the robot's real-time media stream.

#### ConfigRobotRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| configNames | string | configuration parameter name |
| configValue | string | configuration parameter value |

The optional values ​​for configNames are:

| Name | Description |
| --- | --- |
| config_rcuCamera | rcu camera status |
| config_cameras | robot camera status |
| config_topCamera | head camera configuration |
| config_faceDetection | face detection configuration |
| config_tts | tts configuration |
| config_asr | asr configuration |
| config_navigate | navigation settings |
| config_network | network status |

#### RobotConfigResponse

| Parameter Name | Parameter Type |
| --- | --- |
| common_rsp_info | common.CommonRspInfo |

#### GetRobotStateRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| configNames | string | configuration parameter name |
| configValue | string | configuration parameter value |

#### ClientRecordConfig

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| recorder | bool | rcu video recording |
| grab | bool | video capture |
| audio | bool | audio |
| video | bool | video |

Bandwidth string Optional values ​​are low, medium, high

#### ClientRecordStop

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| stopRcuRecord | bool | Whether to turn off rcu video recording |
| stopRtspRecord | bool | Whether to turn off RTSP video recording |

#### ConfigAsrRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| enableAsr | string | true/false Turns off ASRASR |

#### ConfigFrRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| enableFr | string | true/false Turn on image recognition, turn off image recognition |

#### ConfigFaceCamRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| cameraIndex | string | see table below for possible values |

| Camera | number |
| --- | --- |
| 0 | means the overhead camera |
| 1 | means mouth camera |
| 2 | Indicates a chest or RCU camera |

can also be numbered according to the specific robot.

## Robot Navigation Interface
### index

* RobotNavigateService (interface)
  * GetMap (method)
  * GetMapLocation (method)
  * GetPatrolRoute (method)
  * StartPatrol (method)
  * PausePatrol (method)
  * ResumePatrol (method)
  * StopPatrol (method)
  * EnableCalibrationPoints (method)
  * MoveTo (method)
  * MoveByPath (method)
  * SetRobotLocation (method)
* GetMapRequest (message)
* GetPatrolRequest (message)
* NavigateBaseResponse (message)
* PatrolRequest (message)
* EnableCalibrationPointsRequest (message)
* MoveToRequest (message)
* Point (message)
* MoveByPathRequest (message)
* SetRobotLocationRequest (message)

### RobotNavigateService

Provides robot navigation related APIs.

#### GetMap

Rpc GetMap (GetMapRequest) returns (NavigateBaseResponse)
Get robot map information.

#### GetMapLocation

Rpc GetMapLocation (GetMapRequest) returns (NavigateBaseResponse)
Get pre-defined points of interest on the map.

#### GetPatrolRoute

Rpc GetPatrolRoute (GetPatrolRequest) returns (NavigateBaseResponse)

Get patrol path information.

#### StartPatrol

Rpc StartPatrol (PatrolRequest) returns (NavigateBaseResponse)
Start patrol.

#### PausePatrol

Rpc PausePatrol (PatrolRequest) returns (NavigateBaseResponse)
Pause patrol.

#### ResumePatrol

Rpc ResumePatrol (PatrolRequest) returns (NavigateBaseResponse)
Resume patrol.

#### StopPatrol

Rpc StopPatrol (PatrolRequest) returns (NavigateBaseResponse)
Stop patrolling.

#### EnableCalibrationPoints

Rpc EnableCalibrationPoints (EnableCalibrationPointsRequest) returns (NavigateBaseResponse)

Calibration point upload.
Due to the limitation of the robot positioning technology, the robot may lose positioning due to accidental situations. At this time, the robot will not be able to complete the patrol task normally.
Manual assistance is required.

After the calibration point upload function is turned on, the robot will upload the current calibration point, assist the customer service to determine the current position of the robot, and then adjust
Use the SetRobotLocation method to complete the auxiliary positioning.

#### MoveTo

Rpc MoveTo (MoveToRequest) returns (NavigateBaseResponse)
Move to a certain point.

#### MoveByPath

Rpc MoveByPath (MoveByPathRequest) returns (NavigateBaseResponse)
Move a section of the path.

#### SetRobotLocation

Rpc SetRobotLocation (SetRobotLocationRequest) returns (NavigateBaseResponse)
Set the current position of the robot.

### GetMapRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| queryType | string | 0:All maps, 1: Current map, 2: Single designated map |
| mapId | string | Map ID |
| mapName | string | Map name |

### GetPatrolRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| queryType | string | 0:All, 1: Current task, 2: Single designated task |
| mapId | string | Map ID |
| mapName | string | Map name |
| routeId | string | path ID |
| routeName | string | path name |

### NavigateBaseResponse

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| result | string | returned request result |

### PatrolRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| mapId | string | Map ID |
| mapName | string | Map name |
| routeId | string | path ID |
| routeName | string | path name |

#### EnableCalibrationPointsRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| enableCalibrationPoints | bool | true false Turns on checkpoint uploading, closes upload |

#### MoveToRequest

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| common_req_header | common.CommonReqHeader | Generic request header |
| mode | in32 | Patrol mode: 1 move, 2 patrol |
| speed | ​​in32 | speed |
| point | Point | location point information |
| locationId | string | location ID |

#### Point

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| mapId | string | map ID |
| mapName | string | map name |
| x | in32 | X coordinate |
| y | int32 | Y coordinate |

#### MoveByPathRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| mode | in32 | patrol mode: 1 move, 2 patrol |
| patrolTimes | in32 | patrols |
| speed | ​​in32 | speed |
| points | Point[] | path point information |

## SetRobotLocationRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| point | Point | current position of the robot |

## Voice Broadcast Interface

### index

* RobotSpeakService (interface)
  * Speak (method)
* SpeakRequest (message)
* SpeakResponse (message)

### RobotSpeakService

Used to implement the voice that allows the robot to say a specified text.

#### Speak

Rpc kill (SpeakRequest) returns (SpeakResponse)
Say a voice.

### SpeakRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| tts | TTS[] | tts array of data |

### TTS

| parameter name | parameter type | description |
| --- | --- | --- |
| lang | string | text language current support en, zh, jp |
| text | string | the text to be spoken by the robot |
| action | string | The action data accompanying the speech |

### SpeakResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| result | string | returned request result |

## Robot Platform API

* Robot Control
* Robot Configuration
* Robot Navigation
* Voice broadcast
* Event Notification
* Agent Control
* Natural Language Understanding (NLU)

## Manual Auxiliary Interface ( Deprecated )
### index

* RobotOperatorService (interface)
  * SwitchHi (method)
  * UpdateConfidenceIndex (method)
  * UpdateChichat (method)
* SwitchRequest (message)
* ConfidenceRequest (message)
* ChichatRequest (message)

### RobotOperatorService

Used to control and operate manual assist related functions.

#### SwitchHi

Rpc SwitchHi (SwitchRequest) returns (OperatorBaseResponse)
Forced switching of the agent service.

#### UpdateConfidenceIndex

Rpc UpdateConfidenceIndex (ConfidenceRequest) returns (OperatorBaseResponse)
Update confidence value

#### UpdateChichat

Rpc UpdateChichat(ChichatRequest) returns (OperatorBaseResponse)
Turn the chat library on or off.

### SwitchRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| confidenceIndex | in32 | confidence value |
| reason | string | reason for switching |

### OperatorBaseResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| result | string | returned request result |

### ConfidenceRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| confidenceIndex | in32 | confidence value |

### ChichatRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| chichat | string| 1:on, 0:off |

## Robot Platform API

* Natural Language Understanding (NLU)
* Speech Recognition (ASR)
* Image Recognition (CV)

## Natural Language Understanding (NLU)
### index

* NLUService (interface)
  * DetectIntent (method)
  * DetectIntentText (method)
* DetectRequest (message)
* DetectResponse (message)
* DetectIntentTextRequest (message)
* DetectIntentTextResponse (message)
* Body
* TalkType (message)
* NLUType (message)
* DialogflowResponse (message)
* SmartvoiceResponse (message)
* Tree (message)
* Hitlog (message)
* TTS (message)
* Action
* Param (message)

### NLUService

#### DetectIntent

Rpc DetectIntent (DetectRequest) returns (DetectResponse)
Used to implement text intent recognition.

#### DetectIntentText

Rpc DetectIntentText (DetectIntentTextRequest) returns (DetectIntentTextResponse)
Used to implement text intent recognition.

### DetectRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| body | Body | content information |

### DetectResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| result_oneof | DialogflowResponse | | SmartvoiceResponse Resolution Results |
| source | NLUType | pushes large screen information body |
| detail_message | google| .protobuf.Struct tag |

### DetectIntentTextRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| text | string | text to be recognized |
| lang | string | language |
| longitude | string | longitude |
| latitude | string | latitude |

### DetectIntentTextResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| tts | TTS | analysis results |
| push | Push | pushes large screen information body |
| tags | string[] | tag |

### Body

| parameter name | parameter type | description |
| --- | --- | --- |
| text | string | ASR text to be recognized |
| lang | string | language |
| option | map | optional parameter |
| longitude | double | longitude |
| longitude | double | latitude |
| talk_type | TalkType | The selected NLP engine that indicates which method to use for semantic parsing |
| df_extra_params | map | Additional parameters passed in when using Dialogflow |
| sv_extra_params | map | Additional parameters passed in when using SmartVoice, such as face information, greeting information |

### TalkType

This parameter specifies the parsing engine to be used for intent recognition.

| Enumeration Value | Description |
| --- | --- |
| NORMAL | Ordinary QA |
| FIRST_CONNECT | The first connection of the robot |
| WELCOME | People recognize the greetings after the words |
| KEEP_WELCOME | Greetings when people stay the same |
| LEAVE_WELCOME | People leave greetings |

### NLUType

This parameter specifies the parsing engine to be used for intent recognition.

| Enumeration Value | Description |
| --- | --- |
| DEFAULT | Recognition by system default settings |
| DIALOGFLOW | uses the Dialogflow engine for identification |
| SMARTVOICE | uses the SmartVoice engine for identification |

### DialogflowResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| tts | TTS | Analysis Results |
| push | Push | pushes large screen information body |
| tags | string[] | tag |

### SmartvoiceResponse

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| source | string | |
| text | string | recognize text |
| lang | string | language |
| operation | google| .protobuf.Any operation returned by dialogflow |
| iscredible | string | is trusted |
| confidence | int32 | confidence value |
| tts | TTS[] |  |
| tags | string[] | tag |
| recomendation | string[] | recommended |
| simqs | string[] | Similar problems |
| gwdata | string[] |  |
| tree | tree | intent tree |
| expiration | int32 | expiration time |
| hitlog | Hitlog | hit log |

### Tree

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| currentstate | string | current state |
| subtree | string | substate tree |

### Hitlog

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| algo | string | algorithm |
| domain | string | domain |
| domainid | string | domain ID |
| incontext | string | above environment |
| intent | string | intention |
| intentid | string | intent ID |
| outcontext | string | |
| traceId | string | trace ID |

### TTS

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| text | string | text |
| lang | string | language |
| action | Action | |
| emoji | string | |
| payload | string | parameter |
| faceid | string | face information |
| devicetype | string | device type |

### Action

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| name | string | name |
| display | string | display name |
| param | Param | action parameters |

### Param

| Parameter Name | Parameter Type | Description |
| --- | --- | --- |
| duration | int32 | duration |
| url | string | address |
| pic_url | string | image address |
| video_url | string | video address |
| intent | string | intention |
| params | map | parameter |
| raw_data | string | raw data |

## Speech Recognition
### index

* SpeechService (interface)
  * Recognize (method)
  * StreamingRecognize (method)
* RecognitionRequest (message)
* RecognitionResponse (message)
* Body
* Extra (message)
* Type (enumeration)
* Data (message)

### SpeechService

The HARIX-ASR service is a microservice in the HARIX platform that uses an object-oriented gRPC framework to help you convert audio data into text.
Rong. Use HARRI-ASR service to connect to many well-known AI platforms, including Google, Xunfei, Baidu, Daxie, etc., without paying attention to each AI platform.
The difference between the two.

#### Recognize

Rpc Recognize (RecognitionRequest) returns (RecognitionResponse)
Convert a voice clip into text.

#### StreamingRecognize

Rpc StreamingRecognize (stream RecognitionRequest) returns (stream RecognitionResponse)
Streaming speech segments into text in a stream.

### RecognitionRequest:

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| body | Body | speech recognition detailed data |
| extra | Extra | additional data |

### RecognitionResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| result | string | returned request result |

### Body

| parameter name | parameter type | description |
| --- | --- | --- |
| type | Type |  |
| sid | string |unique ID |
| app_type | string | application type |
| tag | string | tag |
| stream_flag | int32 | stream flag |
| option | map | additional data |
| data | Data | additional data |

### Extra

| parameter name | parameter type | description |
| --- | --- | --- |
| extra_type | string | type |
| extra_body | string | parameter value |

### Type

| Enumeration Value | Description |
| --- | --- |
| BLOCK | Block Recognition |
| STREAMING | Streaming Recognition |

### Data

| parameter name | parameter type | description |
| --- | --- | --- |
| rate | int32 | sampling rate always 16000 |
| format | string | pcm |
| account | string | user ID |
| language | string | Language code, optional value CH EN TCH JA ES |
| dialect | string |  |
| vendor | string | parsing engine, optional Google, Baidu, IFlyTek |
| channel | int32 | |
| duration | int32 | duration |
| flag | int32 | |
| speech | bytes | |

## Image recognition interface
### index

* VisionService (interface)
  * Recognize (method)
  * StreamingRecognize (method)
* ImageRequest (message)
* ImageResponse (message)
* Body
* RecognizeType (enumeration)
* PersonStatus (enumeration)
* RecognizeOption (enumeration)
* Extra (message)
* Ocr (message)
* Money (message)
* MoneyInfo (message)
* CarPlate (message)
* Object (message)
* Result (message)
* Label (message)
* Location
* Face (message)
* FaceObject (message)
* Rect (message)
* FaceAttr (message)
* Attribute (message)
* Caption (message)
* Classify (message)
* Fall (message)
* Fall_Result (message)
* Compare (message)

### VisionService

Used to implement image recognition related functions. This service currently supports:

1. 2D image detection
1. 3D object detection

#### Recognize

Rpc Recognize (ImageRequest) returns (ImageResponse)
Image Identification.

#### StreamingRecognize

Rpc StreamingRecognize (stream ImageRequest) returns (ImageResponse)
Streaming image recognition.

### ImageRequest

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | generic request header |
| body | Body | image recognition detailed data |
| extra | Extra | additional data |

### ImageResponse

| parameter name | parameter type | description |
| --- | --- | --- |
| common_req_header | common.commonreqheader | universal return header |
| details_message | google| .protobuf.Struct returned details |
| result_oneof | (see possible values below) |  Returned recognition result |

* String nothing
* Ocr ocr
* Money money
* CarPlate car_plate
* Object object
* Face face
* FaceAttr face_attr
* Caption caption
* Classify classify
* Fall fall
* Compare compare

### Body

| parameter name | parameter type | description |
| --- | --- | --- |
| type | RecognizeType | request identification type |
| image_from_net | bool | Identifies the image to get the image to get the source, whether it is obtained from streaming media or rcu |
| image | bytes | byte[] rcu container for passing images |
| flag | int32 | flag |
| status | PersonStatus | The behavior status of the person passed by the camera |
| option | map | identifies whether the result returns 2.returnDetail returns the recognition result and details rcu 1.recognizeOnly only recognizes |
| recognize_option | RecognizeOption | identifies request suggestions |

### RecognizeType

| Enumeration Value | Description |
| --- | --- |
| OCR | text recognition |
| MONEY | Coin Identification |
| CAR_PLATE | License Plate Recognition |
| OBJECT | Object Recognition |
| FACE | Face Recognition |
| FACE_ATTR | Face attribute recognition |
| CAPTION | Image Scene Recognition |
| CLASSIFY | Image Classification and Recognition |
| FALL | Falling recognition |
| COMPARE | Human Certificate Comparison |

### PersonStatus

| Enumeration Value | Description |
| --- | --- |
| COME | New arrival |
| KEEP | Keep |
| LEAVE | Leave |

### RecognizeOption 

| Enumeration Value | Description |
| --- | --- |
| NEVER | never recognizes |
| ALWAYS | Continuous Recognition |
| ASK | Pre-identification inquiry |


### Extra

| parameter name | parameter type | description |
| --- | --- | --- |
| extra_type | string | type |
| extra_body | string | parameter value |

### Ocr

| parameter name | parameter type | description |
| --- | --- | --- |
| text | string | recognized text |

### Money

| parameter name | parameter type | description |
| --- | --- | --- |
| result | MoneyInfo[] | coin recognition result |

### MoneyInfo

| parameter name | parameter type | description |
| --- | --- | --- |
| class | string | |
| class_en | string | face value - English |
| confidence | string | confidence value |
| sample_image | string | Sample of the corresponding face value coin |

### CarPlate

| parameter name | parameter type | description |
| --- | --- | --- |
| plate | string | |

### Object

| parameter name | parameter type | description |
| --- | --- | --- |
| result | Result | List of identified objects |
| tag | string | tag |

### Result

| parameter name | parameter type | description |
| --- | --- | --- |
| labels | Label[] | label information |
| location | Location[] | Location Information |

### Label

| parameter name | parameter type | description |
| --- | --- | --- |
| color | int32[] | colour |
| name | string | first name|
| name_en | string | name english |

### Location

| parameter name | parameter type | description |
| --- | --- | --- |
| bbox | int32[] | types of |
| color | int32[] | colour |
| name | string | first name |
| name_en | string | name english |

### Face

| parameter name | parameter type | description |
| --- | --- | --- |
| face_num | string | number of faces identified |
| faces | FaceObject | face information |

### FaceObject

| parameter name | parameter type | description |
| --- | --- | --- |
| name | string | name |
| rect | Rect | face area |
| similarity | float | similarity |

### Rect

| parameter name | parameter type | description |
| --- | --- | --- |
| x | int32 | name |
| y | int32 | face area |
| width | int32 | similarity|
| height | int32 | parameter value |

### FaceAttr

| parameter name | parameter type | description |
| --- | --- | --- |
| result | Attribute [] | face attribute information |

### Attribute

| parameter name | parameter type | description |
| --- | --- | --- |
| age | float | age |
| emotion | string | emoticons|
| gender | string | gender |
| rect | Rect | face area |

### Caption

| parameter name | parameter type | description |
| --- | --- | --- |
| caption | string | scene name |
| caption_en | string | scene name in English |

### Classify

| parameter name | parameter type | description |
| --- | --- | --- |
| | | |

### Fall

| parameter name | parameter type | description |
| --- | --- | --- |
| result | Result | fall detection result |

### Fall_Result

| parameter name | parameter type | description |
| --- | --- | --- |
| confidence | float | confindence |
| status | string | status |

### Compare

| parameter name | parameter type | description |
| --- | --- | --- |
| imagefiel1 | Rect | picture 1 area |
| imagefiel2 | Rect | picture 2 area |
| similarity | float | similarity |

## Robot Map API
Map API

### HARIX Map API

### index

* Map related operations
  * Upload map information
  * Page query map information
  * Query map information based on geographic location
  * Get map information details
  * Modify map information
  * Delete map information
* Layer group related operations
  * Paged query layer grouping
  * New layer grouping
  * Modify layer grouping
  * Delete layer grouping
* Layer information related operations
  * New layer information
  * Modify layer information
  * Delete layer information
* Location related operations
  * Query the location by page
  * New location
  * Edit location
  * Delete location

### Map Related Operations

The map service currently only supports HTTP access.

#### Uploading map information

| Basic description | Project description |
| --- | --- |
| Interface Description | Upload Map Information |
| Request Agreement | HTTP |
| Request URL | http://{host-name}/smartmap/portal/map/report/{storageId} |
| Request Method | GET |
| Communication link | RCU -> map service,portal -> map service |
| Remarks | A loading process is required, and the process may wait a while. |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| storageId | String | Required | The storageId returned after the file is uploaded.|

**Request body JSON**

```http://{host-name}/smartmap/portal/map/report/{storageId}```

**Response Body Parameter Description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Response content |

**Response Body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "id": "ea7e77a391bf4487a7480686cf1f36c1",
        "tenantCode": "roc",
        "name": "cloudminds-map",
        "operatorId": "zhangsan",
        "robotId": "system",
        "robotType": "pepper",
        "address": "d537d0d440c4432085428e7b91a99c3f",
        "status": 0 ,
        "createTime": "2019-09-25 22:08:04",
        "updateTime": "2019-09-25 22:08:04",
        "description": null
    }
}
```

** Pagination to query map information**

| Interface Description | Page Query Map Information |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/map/list |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | can fill in | the current page number |
| pageSize | Int | fills in | the number of impressions per page |
| tenantCode | String | Fillable | Tenant |

**Request body JSON**

```json
{
    "pageNum": 1,
    "pageSize": 10,
    "tenantCode": "roc"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use, default "map" |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

The following is business data

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | Required | Page number value |
| pageSize | Int | Required | Number of records per page |
| totalPages | Int | Required | Total number of pages |
| totalRecords | Int | Required | Total Records |
| Records | String | can be filled in | to record details |

The following is the record details

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| map | String | Required | Map information object |
| locationList | String | Required | Location list |
| layerList | String | Required | Layer list |
| layerList.layer | String | Required | Layer |
| layerList.indexList | String | Required | concrete layer file |

**Response Body**

```json
{
    "sys": "MAP",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "pageNum": 1 ,
        "pageSize": 10 ,
        "totalPages": 1 ,
        "total": 2 ,
        "records": [
        {
            "map": {
                "id": "c26079e426e54f1980ffa9d6f95345fd",
                "tenantCode": "roc",
                "name": "cloudminds-map",
                "operatorId": "zhangsan",
                "robotId": "system",
                "robotType": "pepper",
                "address": "d537d0d440c4432085428e7b91a99c3f",
                "status": 0 ,
                "createTime": "2019-09-26 19:57:57",
                "updateTime": "2019-09-26 19:57:57",
                "description": null,
                "locationId": null
            },
            "locationList": [ ],
            "layerList": [
            {
                "layer": {
                    "id": "1",
                    "robotType": "pepper",
                    "layer": 0 ,
                    "name": "底板",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".pgm"
                },
                "indexList": [
                {
                    "id": "07f78b53e74f47ceb6d5c78413eb5268",
                    "mapId": "c26079e426e54f1980ffa9d6f95345fd",
                    "fileName": "fstest.pgm",
                    "address": "04998360a91844828d7aa018326efbc2",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "1"
                }]
            },
            {
                "layer": {
                    "id": "2",
                    "robotType": "pepper",
                    "layer": 1 ,
                    "name": "兴趣点",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".intents"
                },
                "indexList": [
                {
                    "id": "995ce04bdac949e3b28cf032e1480ee9",
                    "mapId": "c26079e426e54f1980ffa9d6f95345fd",
                    "fileName": "fstest.intents",
                    "address": "4a43ff93908546bc841c1557c130a441",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "2"
                }]
            },
            {
                "layer": {
                    "id": "3",
                    "robotType": "pepper",
                    "layer": 2 ,
                    "name": "其他",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".yaml"
                },
                "indexList": [ ]
            }]
        },
        {
            "map": {
                "id": "7ed7439888cd457ca58ba764c29424c3",
                "tenantCode": "roc",
                "name": "cloudminds-map",
                "operatorId": "zhangsan",
                "robotId": "system",
                "robotType": "pepper",
                "address": "d537d0d440c4432085428e7b91a99c3f",
                "status": 0 ,
                "createTime": "2019-09-26 20:55:43",
                "updateTime": "2019-09-26 20:55:43",
                "description": null,
                "locationId": null
            },
            "locationList": [ ],
            "layerList": [
            {
                "layer": {
                    "id": "1",
                    "robotType": "pepper",
                    "layer": 0 ,
                    "name": "底板",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".pgm"
                },
                "indexList": [
                {
                    "id": "fe4a8fe86ac7432d970289019926d5e9",
                    "mapId": "7ed7439888cd457ca58ba764c29424c3",
                    "fileName": "fstest.pgm",
                    "address": "d7b6d9d08bab4d2b826f723bdad86c64",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "1"
                }]
            },
            {
                "layer": {
                    "id": "2",
                    "robotType": "pepper",
                    "layer": 1 ,
                    "name": "兴趣点",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".intents"
                },
                "indexList": [
                {
                    "id": "27720b766809499ba68af1579a71cf45",
                    "mapId": "7ed7439888cd457ca58ba764c29424c3",
                    "fileName": "fstest.intents",
                    "address": "2ea3733cb26849a99bbad07cf96bb89b",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "2"
                }]
            },
            {
                "layer": {
                    "id": "3",
                    "robotType": "pepper",
                    "layer": 2 ,
                    "name": "其他",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".yaml"
                },
                "indexList": [ ]
            }]
        }]
    },
    "errors": null
}
```

### Query map information by location

| Interface Description | Query map information according to geographical location |
| --- | --- |
|Request URL | http://{host-name}/smartmap/portal/map/list/location |
| Request Method | POST |

**Request body parameters**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | can fill in | the current page number |
| pageSize | Int | fills in | the number of impressions per page |

Location String can be filled in

**Request Body**

```json
{
    "pageNum": 1,
    "pageSize": 10,
    "location": "efefefe"
}
```

**Response Body Parameter Description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use, default "map" |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use, default "map" |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

The following is business data

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | Required | Page number value |
| pageSize | Int | Required | Number of records per page |
| totalPages | Int | Required | Total number of pages |
| totalRecords | Int | Required | Total Records |
| records | String | can be filled in | to record details |

The following is the record details

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| map | String | Required | Map information object |
| locationList | String | Required | Location list |
| layerList | String | Required | Layer list |
| layerList.layer | String | Required | Layer |
| layerList.indexList | String | Required | concrete layer file |

**Response body JSON**

```json
{
    "sys": "MAP",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "pageNum": 1 ,
        "pageSize": 10 ,
        "totalPages": 1 ,
        "total": 2 ,
        "records": [
        {
            "map": {
                "id": "c26079e426e54f1980ffa9d6f95345fd",
                "tenantCode": "roc",
                "name": "cloudminds-map",
                "operatorId": "zhangsan",
                "robotId": "system",
                "robotType": "pepper",
                "address": "d537d0d440c4432085428e7b91a99c3f",
                "status": 0 ,
                "createTime": "2019-09-26 19:57:57",
                "updateTime": "2019-09-26 19:57:57",
                "description": null,
                "locationId": null
            },
            "locationList": [ ],
            "layerList": [
            {
                "layer": {
                    "id": "1",
                    "robotType": "pepper",
                    "layer": 0 ,
                    "name": "底板",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".pgm"
                },
                "indexList": [
                {
                    "id": "07f78b53e74f47ceb6d5c78413eb5268",
                    "mapId": "c26079e426e54f1980ffa9d6f95345fd",
                    "fileName": "fstest.pgm",
                    "address": "04998360a91844828d7aa018326efbc2",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "1"
                }]
            },
            {
                "layer": {
                    "id": "2",
                    "robotType": "pepper",
                    "layer": 1 ,
                    "name": "兴趣点",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".intents"
                },
                "indexList": [
                {
                    "id": "995ce04bdac949e3b28cf032e1480ee9",
                    "mapId": "c26079e426e54f1980ffa9d6f95345fd",
                    "fileName": "fstest.intents",
                    "address": "4a43ff93908546bc841c1557c130a441",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "2"
                }]
            },
            {
                "layer": {
                    "id": "3",
                    "robotType": "pepper",
                    "layer": 2 ,
                    "name": "其他",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".yaml"
                },
                "indexList": [ ]
            }]
        },
        {
            "map": {
                "id": "7ed7439888cd457ca58ba764c29424c3",
                "tenantCode": "roc",
                "name": "cloudminds-map",
                "operatorId": "zhangsan",
                "robotId": "system",
                "robotType": "pepper",
                "address": "d537d0d440c4432085428e7b91a99c3f",
                "status": 0 ,
                "createTime": "2019-09-26 20:55:43",
                "updateTime": "2019-09-26 20:55:43",
                "description": null,
                "locationId": null
            },
            "locationList": [ ],
            "layerList": [
            {
                "layer": {
                    "id": "1",
                    "robotType": "pepper",
                    "layer": 0 ,
                    "name": "底板",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".pgm"
                },
                "indexList": [
                {
                    "id": "fe4a8fe86ac7432d970289019926d5e9",
                    "mapId": "7ed7439888cd457ca58ba764c29424c3",
                    "fileName": "fstest.pgm",
                    "address": "d7b6d9d08bab4d2b826f723bdad86c64",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "1"
                }]
            },
            {
                "layer": {
                    "id": "2",
                    "robotType": "pepper",
                    "layer": 1 ,
                    "name": "兴趣点",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".intents"
                },
                "indexList": [
                {
                    "id": "27720b766809499ba68af1579a71cf45",
                    "mapId": "7ed7439888cd457ca58ba764c29424c3",
                    "fileName": "fstest.intents",
                    "address": "2ea3733cb26849a99bbad07cf96bb89b",
                    "status": 0 ,
                    "px": 0 ,
                    "layerId": "2"
                }]
            },
            {
                "layer": {
                    "id": "3",
                    "robotType": "pepper",
                    "layer": 2 ,
                    "name": "其他",
                    "description": null,
                    "status": 0 ,
                    "fileExt": ".yaml"
                },
                "indexList": [ ]
            }]
        }]
    },
    "errors": null
}
```

### Get map information details

| Interface Description |  Get map information details |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/map/detail/{mapId} |
| Request Method | GET |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| mapId | String | Required | Map information ID value |

**Request body JSON**
```
http://{host-name}/smartmap/portal/map/detail/{mapId}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use, the default is map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

The following is business data

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| map | String | Required | Map information object |
| locationList | String | Required | Location list |
| layerList | String | Required | Layer list |
| layerList.layer | String | Required | Layer |
| layerList.indexList | String | Required | concrete layer file |

**Response body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "map": {
            "id": "86b3fedf4d4a4f0bbfef98d13be91196",
            "tenantCode": "roc",
            "name": "cloudminds-map",
            "operatorId": "zhangsan",
            "robotId": "system",
            "robotType": "pepper",
            "address": "b290f403f6554117ad0b9357d31259b3",
            "status": 0 ,
            "createTime": "2019-09-25 22:06:06",
            "updateTime": "2019-09-25 22:06:06",
            "description": null,
            "locationId": "2b772eb0aee940b5a4333bb07471f95a"
        },
        "locationList": [
        {
            "id": "7bc96bfc4022435db68ee56a9363fd6d",
            "parentId": null,
            "name": "T3",
            "status": 0 ,
            "createTime": "2019-09-25 22:06:06",
            "updateTime": "2019-09-25 22:06:06"
        },
        {
            "id": "2b772eb0aee940b5a4333bb07471f95a",
            "parentId": "7bc96bfc4022435db68ee56a9363fd6d",
            "name": "F33",
            "status": 0 ,
            "createTime": "2019-09-25 22:06:06",
            "updateTime": "2019-09-25 22:06:06"
        }],
        "layerList": [
        {
            "layer": {
                "id": "1",
                "robotType": "pepper",
                "layer": 0 ,
                "name": "底板",
                "description": null,
                "status": 0 ,
                "fileExt": ".pgm"
            },
            "indexList": [
            {
                "id": "ed4f22a41bfc4605aed613539a390cde",
                "mapId": "86b3fedf4d4a4f0bbfef98d13be91196",
                "fileName": "fstest.pgm",
                "address": "0c67f466a3a54b2b9b7f70e3a586e3d4",
                "status": 0 ,
                "px": 0 ,
                "layerId": "1"
            }]
        },
        {
            "layer": {
                "id": "2",
                "robotType": "pepper",
                "layer": 1 ,
                "name": "兴趣点",
                "description": null,
                "status": 0 ,
                "fileExt": ".intents"
            },
            "indexList": [
            {
                "id": "853c837159ec463885a37e872893de29",
                "mapId": "86b3fedf4d4a4f0bbfef98d13be91196",
                "fileName": "fstest.intents",
                "address": "bc42079ad7564b819cbde174e2d3180d",
                "status": 0 ,
                "px": 0 ,
                "layerId": "2"
            }]
        }]
    }
}
```

### Modify map information

| Interface Description | Modify map information |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/map/update |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | Map ID value |
| name | String | Required | Map name |
| description | String | can be filled in | - |
| location | String | can be filled in | with a location ID |

**Request body JSON**

```json
{
    "id": "86b3fedf4d4a4f0bbfef98d13be91196",
    "name": "cloudminds-map",
    "description": "test"
}
```

Response body parameter description

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default: map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

### Delete map information

| Interface Description | Delete map information |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/map/delete/{id} |
| Request method | GET |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | ID |

**Request body JSON**

```http://{host-name}/smartmap/portal/map/delete/{id}```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default is map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

**Response body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

## Layer group related operations
### Pagination query layer grouping

| Interface Description | Paging Query Layer Grouping |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/maplayer/list |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | can fill in | the current page number |
| pageSize | Int | fills in | the number of impressions per page |
| robotType | String | Fillable | Device Type |

**Request body JSON**

```json
{
    "pageNum": 1,
    "pageSize": 10,
    "robotType": "ginger"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use, default "map" |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

The following is business data

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | Required | Page number value |
| pageSize | Int | Required | Number of records per page |
| totalPages | Int | Required | Total number of pages |
| totalRecords | Int | Required | Total Records |
| records | String | can be filled in | to record details |

The following is the record details

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| name | String | Required | Name |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "pageNum": 1 ,
        "pageSize": 10 ,
        "totalPages": 1 ,
        "total": 2 ,
        "records": [
        {
            "id": "5",
            "robotType": "ginger",
            "layer": 0 ,
            "name": "底板",
            "description": null,
            "status": 0 ,
            "fileExt": null
        },
        {
            "id": "6",
            "robotType": "ginger",
            "layer": 1 ,
            "name": "其他",
            "description": null,
            "status": 0 ,
            "fileExt": null
        }]
    },
    "errors": null
}
```

### New Layer Grouping

| Interface Description | New Layer Grouping |
| --- | --- |
| Seeking agreement | HTTP |
| Request URL | http://{host-name}/smartmap/api/maplayer/add |
| Request Method | GET |
| Communication link | rcu -> map service,port -> map service |
| Remarks | A loading process is required, and the process may wait a while. |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| robotType | String | Required | Robot type |
| name | String | Required | Type name |
| description | String | Optional | Type Description |
| layer | String | Required | Layers |
| fileExt | String | Optional | Match extension, format ".intents" |

**Request body JSON**

```json
{
    "layer": 1,
    "name": "test",
    "description": "desc",
    "fileExt": "",
    "robotType": "ginger"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Response content |

**Response body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": true,
    "errors": null
}
```

### Modify layer grouping

| Interface Description | Modify Layer Grouping |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/maplayer/update |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | Type ID value |
| name | String | Required | Type name |
| description | String | Optional Type | Description |
| layer | String | Required | Layers |
| fileExt | String | Optional | Match extension, format ".intents" |
| 
**Request body JSON**

```json
{
    "id": "2b0d0caaea4f481cbb1c13aa36c7feea",
    "layer": 11,
    "name": "testtest",
    "description": "desc11",
    "fileExt": ".png"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default: map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

### Delete layer grouping

| Interface Description | Delete Layer Grouping |
| --- | --- |
| Request URL | http://{host-name}/smartmap/portal/maplayer/delete/{id} |
| Request method | GET |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | ID |

**Request body JSON**

```
http://{host-name}/smartmap/portal/maplayer/delete/{id}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default is map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required Business | Data Node |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

## Layer information related operations
### New layer information

| Interface Description | Delete Layer Grouping |
| --- | --- |
| Request Agreement | HTTP |
| Request URL | http://{host-name}/smartmap/api/mapindex/add |
| Request Method | GET |
| Communication link | rcu -> map service,port -> map service |
| Remarks | A loading process is required, and the process may wait a while. |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| mapId | String | Required | Map ID |
| layerId | String | Required | Layer Group ID |
| fileName | String | Required | Name |
| Address | String | Required | File ID, storageId after file upload |

#### Request body JSON:

```json
{
    "mapId": "test",
    "layerId": "desc",
    "fileName": "test",
    "address": "desc"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Response content |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": true,
    "errors": null
}
```

### Modify layer information

| Interface Description | Delete Layer Grouping |
| --- | --- |
| Request Agreement | HTTP |
| Request URL | http://{host-name}/smartmap/portal/mapindex/update |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | Type ID value |
| fileName | String | Required | Name |

**Request body JSON**

```json
{
    "id": "2b0d0caaea4f481cbb1c13aa36c7feea",
    "fileName": "testtest"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default: map |
| code | String | Required | Request response coda |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

### Delete layer information

| Interface Description | Delete layer information |
| --- | --- | 
| Request URL | `http://{host-name}/smartmap/portal/mapindex/delete/{id}` |
| Request method | GET |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | ID |

**Request body JSON**

```
http://{host-name}/smartmap/portal/mapindex/delete/{id}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default is map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

**Response body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

## Location related operations
### Pagination Query Location

| Interface Description | Delete layer information |
| --- | --- | 
| Request URL | `http://{host-name}/smartmap/portal/maplocation/list` |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | can fill in | the current page number | 
| pageSize | Int | fills in | the number of impressions per page |
| parentId | String | can be filled in | the parent directory, the root directory is empty |

**Request body JSON**

```json
{
    "pageNum": 1,
    "pageSize": 10,
    "parentId": "fdsfdsf"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use, default "map" |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

The following is business data

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| pageNum | Int | Required | Page number value |
| pageSize | Int | Required | Number of records per page |
| totalPages | Int | Required | Total number of pages |
| totalRecords | Int | Required | Total Records |
| records | String | can be filled in | to record details |

The following is the record details

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| name | String | Required | Name |

Response body JSON:

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": {
        "pageNum": 1 ,
        "pageSize": 10 ,
        "totalPages": 1 ,
        "total": 2 ,
        "records": [
        {
            "id": "7bc96bfc4022435db68ee56a9363fd6d",
            "parentId": null,
            "name": "T3",
            "status": 0 ,
            "createTime": "2019-09-25 22:06:06",
            "updateTime": "2019-09-25 22:06:06"
        },
        {
            "id": "2b772eb0aee940b5a4333bb07471f95a",
            "parentId": "7bc96bfc4022435db68ee56a9363fd6d",
            "name": "F33",
            "status": 0 ,
            "createTime": "2019-09-25 22:06:06",
            "updateTime": "2019-09-25 22:06:06"
        }]
    },
    "errors": null
}
```

### New location

| Interface Description | Delete layer information |
| --- | --- | 
| Request Agreement | HTTP |
| Request URL | `http://{host-name}/smartmap/api/maplocation/add` |
| Request Method | GET |
| Communication link | rcu -> map service,port - - map service |
| Remarks | need to have loading |

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| name | String | Required |Type name |
| parentId | String | Optional | parent directory, root directory empty |

**Request body JSON**

```json
{
    "name": "test",
    "parentId": "57a7ac81ad5f4442b77ce133bd6cde1c"
}
```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | platform identifier, extended use |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Response content |

**Request body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": true,
    "errors": null
}
```

### Modify location

| Interface Description | Delete layer information |
| --- | --- | 
| Request URL | `http://{host-name}/smartmap/portal/maplocation/update` |
| Request Method | POST |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | ID value |
| name | String | Required | Name |

**Request body JSON**

```json
{
    "id": "e29a0975f36c4e089c7e2c4a509e3bbb",
    "name": "testtest"
}
```

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default: map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

**Request body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

### Delete location

| Interface Description | Delete layer information |
| --- | --- | 
| Request URL | `http://{host-name}/smartmap/portal/maplocation/delete/{id}` |
| Request method | GET |

**Request body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| id | String | Required | ID |

**Request body JSON**

```http://{host-name}/smartmap/portal/maplocation/delete/{id}```

**Response body parameter description**

| Parameter Name | Type | Parameter Limit | Description |
| --- | --- | --- | --- |
| sys | String | can be filled in | the platform identifier, extended use; default is map |
| code | String | Required | Request response code |
| messages | String | can be filled in | Request response information |
| data | String | Required | Business Data Node |

**Request body JSON**

```json
{
    "sys": "ROC",
    "code": 0 ,
    "messages": "OK",
    "data": 1 ,
    "errors": null
}
```

## Download Center
### Name Address Description

HARIX-Client-SDK-Development Guide-3.0 Download Mobile terminal access Harix interface document
Roc_API_ThirdParty_Interface_Design_V1.0.2.pdf Download Mobile terminal access ROC system interface document